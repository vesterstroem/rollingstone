result = "";

function printRandomNotListened(tableId) {
    if (result != "") {
	alert(result);
	return;
    }
    
    notListened = notListenedCount(tableId);
    var client = new HttpClient();
    request = 'https://www.random.org/integers/?num=1&min=1&max='+notListened+'&col=1&base=10&format=plain&rnd=new';
    client.get(request, function(response) {
	index = parseInt(response)-1;
	value = indexToEntry(index, tableId);
	console.log("# of not listened: " + notListened);
	console.log("RANDOM.ORG request: " + request);
	console.log("RANDOM.ORG response: " + response);
	console.log("index: " + index);
	console.log("row: " + value);
	result = "There are " + notListened + " unlistened albums left.\nThe following album has been selected:\n" + value;
	alert(result);
    });
}

function notListenedCount(tableId) {
    var table=document.getElementById(tableId);
    first=true;
    listenedColumnIndex=-1;
    notListenedCount=0;
    cnt = 0;
    for (let row of table.rows) {
	if (first) {
	    first=false;
	    j=0;
	    for (let cell of row.cells) {
		let headerName = cell.innerHTML;
		if (headerName == "Listened") {
		    listenedColumnIndex = j;
		}
		j++;
	    }
	} else {
	    let val = row.getElementsByTagName("td")[listenedColumnIndex].innerHTML;
	    if (val == "") {
		notListenedCount++;
	    }
	}
	cnt++;
    }
    return notListenedCount;
}

function indexToEntry(index, tableId) {
    var table=document.getElementById(tableId);
    first=true;
    notListenedCount=0;
    for (let row of table.rows) {
	if (first) {
	    first=false;
	    j=0;
	    for (let cell of row.cells) {
		let headerName = cell.innerHTML;
		if (headerName == "Listened") {
		    listenedColumnIndex = j;
		}
		j++;
	    }
	} else {
	    let val = row.getElementsByTagName("td")[listenedColumnIndex].innerHTML;
	    if (val == "") {
		if (notListenedCount == index) {
		    let val =
			row.getElementsByTagName("td")[0].innerHTML + " - " +
			row.getElementsByTagName("td")[1].innerHTML + " - " +
			row.getElementsByTagName("td")[2].innerHTML + " - " +
			row.getElementsByTagName("td")[3].innerHTML
			;
		    return val;
		}
		notListenedCount++;
	    }
	}
    }
    return "";
}

var HttpClient = function() {
    this.get = function(aUrl, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function() { 
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                aCallback(anHttpRequest.responseText);
        }
        anHttpRequest.open( "GET", aUrl, true );            
        anHttpRequest.send( null );
    }
}
